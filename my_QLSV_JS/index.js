// ---Array dssv---
var dssv = [];
// ---localstorage---
// ---khai báo DSSV_Local---
const QLSV = "DSSV";
// em 0a l[u local 02u mà nó có???]
var dataJson = localStorage.getItem(QLSV);
if (dataJson != null) {
  dssv = JSON.parse(dataJson).map(function (item) {
    // item : là phẩn tử của array trong các lần lặp
    return new SinhVien(
      item.maSv,
      item.tenSv,
      item.emailSv,
      item.passSv,
      item.diemToan,
      item.diemLy,
      item.diemHoa
    );
  });
  renderDSSV();
}
// -----function thêm sinh viên------
function btnthemSV() {
  // ---input---
  var maSV = document.getElementById("txtMaSV").value;
  var tenSv = document.getElementById("txtTenSV").value;
  var emailSV = document.getElementById("txtEmail").value;
  var passSV = document.getElementById("txtPass").value;
  var diemToanSV = document.getElementById("txtDiemToan").value * 1;
  var diemLySV = document.getElementById("txtDiemLy").value * 1;
  var diemHoaSV = document.getElementById("txtDiemHoa").value * 1;
  // ---creat OOP DSSV---
  var sv = {
    ma: maSV,
    ten: tenSv,
    email: emailSV,
    passSv: passSV,
    diemToan: diemToanSV,
    diemLy: diemLySV,
    diemHoa: diemHoaSV,
  };
  dssv.push(sv);
  let dataJson = JSON.stringify(dssv);

  localStorage.setItem("DSSV", dataJson);
  // ---logic for loop function them sinh vien---
  resetform();
  var contentHTML = "";
  for (i = 0; i < dssv.length; i++) {
    // ---khai báo biến itemSv gán giá trị [i] vào---
    var itemSV = dssv[i];
    var contentTr = `<tr>
    <td>${itemSV.ma}</td>
    <td>${itemSV.ten}</td>
    <td>${itemSV.email}</td>
    <td>0</td>
    <td>
    <button type="button" onclick="xoaSv('${itemSV.ma}')" id="deteleSV" class="btn btn-danger">Xóa</button>
    <button type="button" onclick="SuaSv(${itemSV.ma})" id="suaSV" class="btn btn-warning">Sửa</button>
    </td>
    </tr>`;
    contentHTML += contentTr;
  }
  document.getElementById("tbodySinhVien").innerHTML = contentHTML;
}
// ---------function Xóa QLSV-------
function xoaSv(id) {
  viTri = -1;
  for (let i = 0; i < dssv.length; i++) {
    if (dssv[i].ma == id) {
      viTri = i;
    }
    if (viTri != -1) {
      // console.log(dssv.length);
      dssv.splice(viTri, 1);
      // console.log(dssv.length);
      renderDSSV(dssv);
    }
  }
  // console.log("id:Sang ", id);
}
//---------function Sửa QLSV---------
function SuaSv(id) {
  console.log("id: ", id);
  var viTri = dssv.findIndex(function (itemSV) {
    return itemSV.ma == id;
  });
  // console.log("viTri: ", viTri);
  //----show thông tin lên form----
  var sv = dssv[viTri];
  // console.log("sv sun: ", sv);
  document.getElementById("txtMaSV").value = sv.ma;
  document.getElementById("txtTenSV").value = sv.ten;
  document.getElementById("txtEmail").value = sv.email;
  document.getElementById("txtPass").value = sv.pass;
  document.getElementById("txtDiemToan").value = sv.diemToan;
  document.getElementById("txtDiemLy").value = sv.diemLy;
  document.getElementById("txtDiemHoa").value = sv.diemHoa;
}
//-------lấy thông tin từ form để cập nhật Sv-------
function layThongTinTuForm() {
  var maSv = document.getElementById("txtMaSV").value;
  var tenSv = document.getElementById("txtTenSV").value;
  var emailSv = document.getElementById("txtEmail").value;
  var passSv = document.getElementById("txtPass").value;
  var diemToan = Number(document.getElementById("txtDiemToan").value);
  var diemLy = Number(document.getElementById("txtDiemLy").value);
  var diemHoa = Number(document.getElementById("txtDiemHoa").value);
  //--------------------------------------------------------------------
  //create OOP SV
  // return {
  //   ma: maSv,
  //   ten: tenSv,
  //   email: emailSv,
  //   passSv: passSv,
  //   diemToan: diemToan,
  //   diemLy: diemLy,
  //   diemHoa: diemHoa,
  // };
  return new SinhVien(maSv, tenSv, emailSv, passSv, diemToan, diemLy, diemHoa);
}
function SinhVien(ma, ten, email, passSv, diemToan, diemLy, diemHoa) {
  this.ma = ma;
  this.ten = ten;
  this.email = email;
  this.passSv = passSv;
  this.diemToan = diemToan;
  this.diemLy = diemLy;
  this.diemHoa = diemHoa;
  this.tinhDTB = function () {
    var dtb = (diemToan + diemLy + diemHoa) / 3;
    // console.log("dtb: ", dtb);
    return dtb;
  };
}

//-------cập nhật SV----------------
function capnhatSv() {
  // console.log("sunn");
  var sv = layThongTinTuForm();
  console.log("sv: ", sv);
  var viTri = dssv.findIndex(function (item) {
    return item.ma == sv.ma;
  });
  console.log("viTri: ", viTri);

  dssv[viTri] = {
    ma: sv.ma,
    ten: sv.ten,
    email: sv.email,
    passSv: sv.passSv,
    diemToan: sv.diemToan,
    diemLy: sv.diemLy,
    diemHoa: sv.diemHoa,
  };

  console.log("dssv[viTri]: ", dssv[viTri]);
  console.log("dssv: ", dssv);
  renderDSSV(dssv);
}
function resetform() {
  document.getElementById("formQLSV").reset();
}
