function renderDSSV(dssv) {
  var contentHTML = "";
  for (i = dssv.length - 1; i >= 0; i--) {
    var itemSv = dssv[i];
    var contentTr = `
    <tr>
    <td>${itemSv.ma}</td>
    <td>${itemSv.ten}</td>
    <td>${itemSv.email}</td>
    <td>0</td>
    <td>
    <button type="button" onclick="xoaSv(${itemSv.ma})" id="deteleSV" class="btn btn-danger">Xóa</button>
    <button type="button" onclick="SuaSv(${itemSv.ma})" id="suaSV" class="btn btn-warning">Sửa</button>
    </td>
    </tr>
    `;
    contentHTML += contentTr;
  }
  document.getElementById("tbodySinhVien").innerHTML = contentHTML;
}
