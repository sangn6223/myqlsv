function renderDSSV(dssv) {
  var contentHTML = "";
  for (i = dssv.length - 1; i >= 0; i--) {
    var item = dssv[i];
    var contentTr = `
      <tr>
      <td>${item.ma}</td>
      <td>${item.ten}</td>
      <td>${item.email}</td>
      <td>${item.tinhDTB()}</td>
      <td>
      <button  class="btn btn-danger" id="deleteSv" onclick="xoaSV(${
        item.ma
      })">Xóa</button>
      <button class="btn btn-success" id="SuaSv" onclick="SuaSv(${
        item.ma
      })">Sửa</button>
      </td>
      <td></td>
      </tr>
      `;
    contentHTML += contentTr;
  }
  document.getElementById("tbodySinhVien").innerHTML = contentHTML;
}
function layThongTinTuForm() {
  var maSv = document.getElementById("txtMaSV").value;
  var tenSv = document.getElementById("txtTenSV").value;
  var emailSv = document.getElementById("txtEmail").value;
  var passSv = document.getElementById("txtPass").value;
  var diemToan = Number(document.getElementById("txtDiemToan").value);
  var diemLy = Number(document.getElementById("txtDiemLy").value);
  var diemHoa = Number(document.getElementById("txtDiemHoa").value);

  //--------------------------------------------------------------------
  //create OOP SV
  // return {
  //   ma: maSv,
  //   ten: tenSv,
  //   email: emailSv,
  //   passSv: passSv,
  //   diemToan: diemToan,
  //   diemLy: diemLy,
  //   diemHoa: diemHoa,
  // };
  return new SinhVien(maSv, tenSv, emailSv, passSv, diemToan, diemLy, diemHoa);
}
