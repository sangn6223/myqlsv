// lấy thông tin từ form
// tạo mảng array để lưu trữ value
var dssv = [];
const DSSV_LOCAL = "DSSV_LOCAL_Sun"; //"DSSV_LOCAL này e tìm mà ko thấy ở đâu--em bật src" -- e ko có thấy nè -- cái này nó sẽ hện tr3n browser--là cái này browser cho mình fk a -- mình 0ặt t3n nó là "DSSV_LOCAL" thì browser sẽ hện "DSSV_LOCAL"--
//khi user load trang => lấy dữ liệu localstorage
// 1 vẫn ko tìm thấy :() e chỉ cần biết nó từ đâu ra thôi--- e thử lại xem s
// e làm bên index
var dataJson = localStorage.getItem(DSSV_LOCAL); // lấy data từ local
// là mình tạo "biến local"???????? ở đây fk a
if (dataJson != null) {
  dssv = JSON.parse(dataJson).map(function (item) {
    // item : là phẩn tử của array trong các lần lặp
    return new SinhVien(
      item.maSv,
      item.tenSv,
      item.emailSv,
      item.passSv,
      item.diemToan,
      item.diemLy,
      item.diemHoa
    );
  });
  renderDSSV(dssv);
}
// console.log("dataJson: ", dataJson);
function btnthemSV() {
  //  revert nao
  // // input
  var maSv = document.getElementById("txtMaSV").value;
  var tenSv = document.getElementById("txtTenSV").value;
  var emailSv = document.getElementById("txtEmail").value;
  var passSv = document.getElementById("txtPass").value;
  var diemToan = Number(document.getElementById("txtDiemToan").value);
  var diemLy = Number(document.getElementById("txtDiemLy").value);
  var diemHoa = Number(document.getElementById("txtDiemHoa").value);
  //    var dtb = 0;
  // --------------------------------------------------------------------
  // create OOP SV
  var sv = {
    ma: maSv,
    ten: tenSv,
    email: emailSv,
    passSv: passSv,
    diemToan: diemToan,
    diemLy: diemLy,
    diemHoa: diemHoa,
  };
  //------kiểm tra trùng------
  // kiểm tra mã
  var isValid =
    kiemTraTrung(sv.ma, dssv) && kiemTraDoDai(sv.ma, "spanMaSV", 4, 6);
  // kiểm tra Email
  isValid = isValid & kiemTraEmail(sv.email);

  if (isValid) {
    // var sv = layThongTinTuForm();
    dssv.push(sv);
    // --------convert data JSON-------
    let dataJson = JSON.stringify(dssv);
    //--------lưu vào localstorage----
    localStorage.setItem("DSSV_LOCAL", dataJson);
    //-----------------------------------------------------------------------
    // push() : thêm phần tử vào array dssv
    //--------------------------
    // render dssv lên table
    renderDSSV(dssv);
    resetform();
  }

  // tbodySinhVien
  //   var contentHTML = "";
  //   for (i = 0; i < dssv.length; i++) {
  //     var item = dssv[i];
  //     var contentTr = `
  //     <tr>
  //     <td>${item.ma}</td>
  //     <td>${item.ten}</td>
  //     <td>${item.email}</td>
  //     <td>0</td>
  //     <td>
  //     <button class="btn btn-danger" id="deleteSv" onclick="xoaSV(${item.ma})">Xóa</button>
  //     </td>
  //     <td></td>
  //     </tr>
  //     `;
  //     contentHTML += contentTr;
  //   }
  //   document.getElementById("tbodySinhVien").innerHTML = contentHTML;
  // }
}
// -------------------------------------------
function xoaSV(id) {
  // console.log("xoaSV: ", id);
  //splice: cut ,slice: copy
  // use splice cut
  // slice : copy
  var viTri = -1;
  // nếu tìm thấy vị trí thì xóa
  for (let i = 0; i < dssv.length; i++) {
    if (dssv[i].ma == id) {
      viTri = i;
    }
    if (viTri != -1) {
      // console.log("before dssv: ", dssv.length);
      dssv.splice(viTri, 1);
      // console.log("after dssv: ", dssv.length);
      renderDSSV(dssv);
    }
  }

  //   console.log("viTri: ", viTri);
}
//---------sửa dssv--------
function SuaSv(id) {
  // console.log("id: ", id);
  var viTri = dssv.findIndex(function (item) {
    return item.ma == id;
  });
  // console.log("viTri: ", viTri);
  //----show thông tin lên form----
  var sv = dssv[viTri];
  document.getElementById("txtMaSV").value = sv.ma;
  document.getElementById("txtTenSV").value = sv.ten;
  document.getElementById("txtEmail").value = sv.email;
  document.getElementById("txtPass").value = sv.passSv;
  document.getElementById("txtDiemToan").value = sv.diemToan;
  document.getElementById("txtDiemLy").value = sv.diemLy;
  document.getElementById("txtDiemHoa").value = sv.diemHoa;
}

// --- cập nhật sinh viên ----
function capnhatSv() {
  // console.log("sunn");
  var sv = layThongTinTuForm();
  var viTri = dssv.findIndex(function (item) {
    return item.ma == sv.ma;
  });
  dssv[viTri] = sv;
  renderDSSV(dssv);
}
//localstorage : lưu trữ
//JSON : Convert Data
function resetform() {
  document.getElementById("formQLSV").reset();
}
